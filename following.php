
<?php
include("function.php");
session_start();
$username=$_SESSION['username'];
$accid=$_SESSION['accid'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tiger Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="./css/home.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Tiger Blog</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="profile.php">Profile</a>
                    </li>
                    <li><a href="blogpost.php">Blog Post</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="profile.php">Hello,<?php echo get_firstname($username); ?></a></li>
                    <li><a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-lg-8">

                    <h2 class="page-header">Following</h2>
                    <?php
                    $query="SELECT * from follow f left outer join account a on f.faccid=a.accid where f.accid=$accid";
                    $result = mysql_query( $query );
                    if(!$result)
                    {
                        die ("Could not query the media table in the database: <br />". mysql_error());
                    }
                    while ($result_row = mysql_fetch_assoc($result))
                    {
                        ?>
                        <h4><a href="<?php echo "user.php?method=get&accid=".$result_row['faccid'];?>">
                            <?php echo $result_row['firstname']." ".$result_row['lastname'];?></a>

                     <?php 
                 }
                     ?>
            
            </div>
           
             
            <div class="col-lg-4">
                <div class="well">
                    <h4>Blog Search</h4>
                     <form action="searchblog.php" method=get>
                    <div class="input-group">
                        <form action="searchblog.php" method=get>
                        <input type="text" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                   
                    </div>
                     </form>
                    <!-- /input-group -->
                </div>
                <!-- /well --><?php 
                $query2 = "SELECT * FROM  `blog` GROUP BY `category` ORDER BY COUNT( `category` ) DESC LIMIT 0 , 8"; 
            
                    $result2 = mysql_query( $query2 );
                    if (!$result2)
                        {
                             die ("Could not query the media table in the database: <br />". mysql_error());
                        }
                            
                ?>


                <div class="well">
                    <h4>Popular Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php 
                                for($x=0; $x<=4; $x++)
                               
                            {   $result_row2 = mysql_fetch_assoc($result2);
                                ?>
                                <li><a href="#dinosaurs"><?php echo $result_row2['category']; ?></a>
                                </li>
                               <?php } ?>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php 
                                 while($result_row2 = mysql_fetch_assoc($result2))
                            {  ?>
                                <li><a href="#alien-abductions"><?php echo $result_row2['category']; ?></a>
                                </li>
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Bootstrap's default wells work great for side widgets! What is a widget anyways...?</p>
                </div>
                <!-- /well -->
            </div>
        </div>

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 624 System Admin</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>