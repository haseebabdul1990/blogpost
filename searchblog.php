<?php
include("function.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Post Template for Bootstrap 3</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="./css/home.css" rel="stylesheet">

</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Start bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="profile.php">Profile</a>
                    </li>
                    <li><a href="blogpost.php">Blog Post</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-lg-8">
        
            <div class="row">
              <?php
$types = array();

if (isset($_GET['search'])) {
$search=$_GET['search'];
   $searchTerms1 = str_replace("'", '', $search);
    $searchTerms2 = preg_replace('/[^a-zA-Z0-9" "\']/', '', $searchTerms1);
$searchTerms2 = preg_replace( "/\s+/", " ", $searchTerms2 );
    $searchTerms = strip_tags($searchTerms2);
    $searchTermDB = mysql_real_escape_string($searchTerms);
    $types=explode(" ", $searchTermDB);
    foreach ($types as &$value)
   $value = " `category` LIKE '%{$value}%' ";

        $searchSQL = "SELECT * FROM `blog` NATURAL JOIN `account` WHERE";
        

        $searchSQL .= implode(" OR ", $types) . " limit 0,10";



        $searchResult = mysql_query($searchSQL) or trigger_error("Error!<br/>" . mysql_error() . "<br />SQL Was: {$searchSQL}");
        ?>

                    <h2>search Results:</h2>
                    <?php
           while($result_row1 = mysql_fetch_assoc($searchResult))
                            {

                            ?>  

                           
                                  <p>
                                 <h3><a href=<?php echo "home.php?method=get&blogid1=".$result_row1['blogid'];?> > <?php echo $result_row1['tittle']; ?> </a>           Author:  <?php echo $result_row1['firstname'].$result_row1['lastname']; ?>           
                                 	<a href=<?php echo "home.php?method=get&blogid1=".$result_row1['blogid'];?> > view blog </a>
                                 </h3></br>
                             </P>
                        
                          <?php 
                           }
        if (mysql_num_rows($searchResult) < 1) {
            echo "No Results";
}
    

}

?>

               
             
        
             </div>
             
         </div>
           
             
            <div class="col-lg-4">
                <div class="well">
                    <h4>Blog Search</h4>
                     <form action="searchblog.php" method=get>
                    <div class="input-group">
                        <form action="searchblog.php" method=get>
                        <input type="text" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                   
                    </div>
                     </form>
                    <!-- /input-group -->
                </div>
                <!-- /well -->
                <div class="well">
                    <h4>Popular Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#dinosaurs">Dinosaurs</a>
                                </li>
                                <li><a href="#spaceships">Spaceships</a>
                                </li>
                                <li><a href="#fried-foods">Fried Foods</a>
                                </li>
                                <li><a href="#wild-animals">Wild Animals</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#alien-abductions">Alien Abductions</a>
                                </li>
                                <li><a href="#business-casual">Business Casual</a>
                                </li>
                                <li><a href="#robots">Robots</a>
                                </li>
                                <li><a href="#fireworks">Fireworks</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Bootstrap's default wells work great for side widgets! What is a widget anyways...?</p>
                </div>
                <!-- /well -->
            </div>
        </div>

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 624 System Admin</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>


</body>

</html>
