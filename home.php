
<?php
include("function.php");
session_start();
$username=$_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="./css/home.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Tiger Blog</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="profile.php">Profile</a>
                    </li>
                    <li><a href="blogpost.php">Blog Post</a>
                    </li>
                    <li><a href="myblogs.php">My Blogs</a>
                    </li>
                    <li><a href="following.php">Following Bloggers</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="profile.php">Hello,<?php echo get_firstname($username); ?></a></li>
                    <li><a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-lg-8">
            <?php  
            $result= null ;
            if(isset($_GET['blogid1']))
               {
                 $search= $_GET['blogid1'];
                 $query = "SELECT * FROM  `blog` Natural JOIN  `account` where blogid=". $search;
                $result = mysql_query( $query );
                unset($_GET['blogid1']);
               
            }

            else {
            
                $query = "SELECT * FROM  `blog` Natural JOIN  `account` ORDER BY TIME DESC LIMIT 0, 4"; 
                $result = mysql_query( $query );
            }
                if (!$result)
                {
                    die ("Could not query the media table in the database: <br />". mysql_error());
                }

                 while($result_row = mysql_fetch_assoc($result))
                {
            ?>
            <div class="row">

                <!-- the actual blog post: title/author/date/content -->
                <h1><?php echo $result_row['tittle'];?></h1>
                <p class="lead">by <a href="<?php echo "user.php?accid=".$result_row['accid'];?>"><?php echo $result_row['firstname']." ".$result_row['lastname']; ?></a>
                </p>
                <hr>
                <p>
                    <span class="glyphicon glyphicon-time"></span> Posted on <?php echo $result_row['time'] ?></p>
                <hr>
                <img src="<?php echo $result_row['filepath'].$result_row['filename']; ?>" class="img-responsive" style=" height:300px;width:900px;">
                <hr>
                <p><?php echo $result_row['content'] ?></p>

                <hr>

                <!-- the comment box -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post" action="./script/comment.php">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>
                        <input type="hidden" name="blogid" value="<?php echo $result_row['blogid'];?>">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                

                <hr>

                <!-- the comments -->
              <?php  $query1 = "SELECT * FROM  `comment` Natural JOIN `account` WHERE blogid = ". $result_row['blogid'] ." ORDER BY TIME DESC LIMIT 0 , 30"; 
                    $result1 = mysql_query( $query1 );
                    if (!$result1)
                        {
                             die ("Could not query the media table in the database: <br />". mysql_error());
                        }
                            while($result_row1 = mysql_fetch_assoc($result1))
                            {
                ?>
                                <h3><?php echo $result_row1['firstname']." ".$result_row1["lastname"]; ?>
                                <small><?php echo $result_row1['time'];?></small></h3>
                                <p> <?php echo $result_row1['comment']; ?></p>
               
            
            <?php }  ?>
             </div>
             <?php } ?>
         </div>
           
             
            <div class="col-lg-4">
                <div class="well">
                    <h4>Blog Search</h4>
                     <form action="searchblog.php" method=get>
                    <div class="input-group">
                        <form action="searchblog.php" method=get>
                        <input type="text" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                   
                    </div>
                     </form>
                    <!-- /input-group -->
                </div>
                <!-- /well --><?php 
                $query2 = "SELECT * FROM  `blog` GROUP BY `category` ORDER BY COUNT( `category` ) DESC LIMIT 0 , 8"; 
            
                    $result2 = mysql_query( $query2 );
                    if (!$result2)
                        {
                             die ("Could not query the media table in the database: <br />". mysql_error());
                        }
                            
                ?>


                <div class="well">
                    <h4>Popular Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php 
                                for($x=0; $x<=4; $x++)
                               
                            {   $result_row2 = mysql_fetch_assoc($result2);
                                ?>
                                <li><a href="#dinosaurs"><?php echo $result_row2['category']; ?></a>
                                </li>
                               <?php } ?>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php 
                                 while($result_row2 = mysql_fetch_assoc($result2))
                            {  ?>
                                <li><a href="#alien-abductions"><?php echo $result_row2['category']; ?></a>
                                </li>
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Bootstrap's default wells work great for side widgets! What is a widget anyways...?</p>
                </div>
                <!-- /well -->
            </div>
        </div>

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 624 System Admin</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>


</body>

</html>