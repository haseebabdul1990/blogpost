blogpost
======

blogging application demonstrating double gaurd security feature by mapping session request to database request and identifying potential threats and logging their IP Addresses.
Security from SQL Injection, session hijacking and privilege escalation. 