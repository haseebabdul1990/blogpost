<?php
session_start();
include_once "function.php";
include_once "sql.php";
$accid=$_SESSION['accid'];

$query = "SELECT * from account where accid='$accid'"; 
$result = mysql_query( $query );
if(!$result)
{
  die ("Could not query the media table in the database: <br />". mysql_error());
}
while ($result_row = mysql_fetch_assoc($result))
{
  $uname=$result_row['username'];
  $firstname=$result_row['firstname'];
  $lastname=$result_row['lastname'];
  $email=$result_row['email'];
  $dob=$result_row['dob'];
  $sex=$result_row['sex'];
  $about=$result_row['about'];
  $filename=$result_row['afilename'];
  $filepath=$result_row['afilepath'];
}

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
  </head>

  <body style="padding-top:50px;">

      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Start Bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="profile.php">Profile</a>
                    </li>
                    <li><a href="blogpost.php">Blog Post</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">

      <div class="starter-template">
        <h1>Profile</h1>
        <span id="profile">
          <img src="<?php echo $filepath.$filename; ?>" class="img-responsive" style=" height:200px;width:300px;">
          <h3 align="left">Firstname : <small><?php echo $firstname; ?></small></h3>
          <h3 align="left">Lastname : <small><?php echo $lastname; ?></small></h3>
          <h3 align="left">Username : <small><?php echo $uname; ?></small></h3>
          <h3 align="left">Email : <small><?php echo $email; ?></small></h3>
          <h3 align="left">Date of Birth : <small><?php echo $dob; ?></small></h3>
          <h3 align="left">Sex : <small><?php echo $sex; ?></small></h3>
          <h3 align="left">About : <small><?php echo $about; ?></small></h3>
          <button class="btn btn-primary" id="btnmod">Update</button>
        </span> 

        <span id="updateform" style="display:none;">
          <form role="form" method="post" action="./script/updateprofile.php" style="max-width:300px;">
            <div class="form-group">
              <label for="firstname">Firstname</label>
              <input type="text" class="form-control" name="firstname" value="<?php echo $firstname;?>">
              <label for="lastname">Lastname</label>
              <input type="text" class="form-control" name="lastname" value="<?php echo $lastname;?>">
              <label for="email">Email</label>
              <input type="email" class="form-control" name="email" value="<?php echo $email;?>">
              <label for="dob">Date of Birth</label>
              <input type="date" class="form-control" name="dob" value="<?php echo $dob;?>">
              <label for="about">About</label>
              <textarea class="form-control" name="about" style="width:500px;height:400px"><?php echo $about;?></textarea>
              <button class="btn btn-primary" type="submit" id="btnupdate">Update</button>
            </div>
        </form>
      </span>






      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#btnmod").click(function(){
        $("#profile").hide();
        $("#updateform").show();
      });
    });
     </script>
  </body>
</html>