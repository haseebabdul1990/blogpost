
<?php
include("function.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Post Template for Bootstrap 3</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="./css/home.css" rel="stylesheet">

</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Start Bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="profile.php">Profile</a>
                    </li>
                    <li><a href="blogpost.php">Blog Post</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-lg-8">
            <?php
                $i=0;
                $j=$i + 3;
                $query = "SELECT * FROM  `blog` FULL JOIN  `account` ORDER BY TIME DESC LIMIT $i, $j"; 
                $result = mysql_query( $query );
                if (!$result)
                {
                    die ("Could not query the media table in the database: <br />". mysql_error());
                }

                 while($result_row = mysql_fetch_assoc($result))
                {
            ?>
            <div class="row">
                <!-- the actual blog post: title/author/date/content -->
                <h1><?php echo $result_row['tittle'];?></h1>
                <p class="lead">by <a href="index.php"><?php echo $result_row['firstname']." ".$result_row['lastname']; ?></a>
                </p>
                <hr>
                <p>
                    <span class="glyphicon glyphicon-time"></span> Posted on <?php echo $result_row['time'] ?></p>
                <hr>
                <img src="/blogpost/images/SNORT.jpg" class="img-responsive" style=" height:300px;width:900px;">
                <hr>
                <p><?php echo $result_row['content'] ?></p>
                <ul>
                    <li><a href="http://spaceipsum.com/">Space Ipsum</a>
                    </li>
                    <li><a href="http://cupcakeipsum.com/">Cupcake Ipsum</a>
                    </li>
                    <li><a href="http://tunaipsum.com/">Tuna Ipsum</a>
                    </li>
                </ul>

                <hr>

                <!-- the comment box -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post" action="./script/comment.php">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>
                        <input type="hidden" name="blogid" value="<?php echo $result_row['blogid'];?>">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                

                <hr>

                <!-- the comments -->
              <?php  $query1 = "SELECT * FROM  `comment` FULL JOIN `account` WHERE blogid = ". $result_row['blogid'] ." ORDER BY TIME DESC LIMIT 0 , 30"; 
                    $result1 = mysql_query( $query1 );
                    if (!$result1)
                        {
                             die ("Could not query the media table in the database: <br />". mysql_error());
                        }
                            while($result_row1 = mysql_fetch_assoc($result1))
                            {
                ?>
                                <h3><?php echo $result_row1['firstname']." ".$result_row1["lastname"]; ?>
                                <small><?php echo $result_row1['time'];?></small></h3>
                                <p> <?php echo $result_row1['comment']; ?></p>
               
            
            <?php }  ?>
             </div>
             <?php } ?>
         </div>
           
             
            <div class="col-lg-4">
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </div>
                <!-- /well -->
                <div class="well">
                    <h4>Popular Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#dinosaurs">Dinosaurs</a>
                                </li>
                                <li><a href="#spaceships">Spaceships</a>
                                </li>
                                <li><a href="#fried-foods">Fried Foods</a>
                                </li>
                                <li><a href="#wild-animals">Wild Animals</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#alien-abductions">Alien Abductions</a>
                                </li>
                                <li><a href="#business-casual">Business Casual</a>
                                </li>
                                <li><a href="#robots">Robots</a>
                                </li>
                                <li><a href="#fireworks">Fireworks</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Bootstrap's default wells work great for side widgets! What is a widget anyways...?</p>
                </div>
                <!-- /well -->
            </div>
        </div>

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 624 System Admin</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>


</body>

</html>