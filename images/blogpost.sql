-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2014 at 04:15 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blogpost`
--
CREATE DATABASE IF NOT EXISTS `blogpost` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `blogpost`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `accid` int(10) NOT NULL AUTO_INCREMENT,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `aimagepath` varchar(50) NOT NULL,
  PRIMARY KEY (`accid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`accid`, `firstname`, `lastname`, `email`, `username`, `password`, `aimagepath`) VALUES
(1, 'Shoab', 'Ahmed', 'shoaba@clemson.edu', 'shoab101', '123456', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `blogid` int(10) NOT NULL AUTO_INCREMENT,
  `accid` int(10) NOT NULL,
  `title` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bimagepath` varchar(50) NOT NULL,
  `category` text NOT NULL,
  PRIMARY KEY (`blogid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blogid`, `accid`, `title`, `content`, `time`, `bimagepath`, `category`) VALUES
(1, 1, 'snort', 'We welcome the introduction of the newest rule release from the VRT. In this release we introduced 26 new rules and made modifications to 4 additional rules. \r\n\r\nThere were no changes made to the snort.conf in this release.\r\n\r\nIn VRT''s rule release: \r\nThe Sourcefire VRT has added and modified multiple rules in the blacklist, exploit-kit, file-flash, file-identify, file-multimedia, malware-backdoor, malware-cnc and pua-toolbars rule sets to provide coverage for emerging threats from these technologies.\r\n\r\n\r\nIn order to subscribe now to the VRT''s newest rule detection functionality, you can subscribe for as low as $29 US dollars a year for personal users, be sure and see our business pricing as well at http://www.snort.org/store. Make sure and stay up to date to catch the most emerging threats!', '2014-04-22 20:49:04', 'SNORT.jpg', 'computer science'),
(2, 1, 'security', 'Websense® Web Security blocks web threats to reduce malware infections, decrease help desk incidents and free up valuable IT resources. It has more than 100 security and filtering categories, hundreds of web application and protocol controls, and 60-plus reports with customization and role-based access. You can easily upgrade to Web Security Gateway when desired to get social media controls, SSL inspection, data loss prevention (DLP) and inline, real-time security from Websense ACE (Advanced Classification Engine).', '2014-04-22 21:43:50', '', 'computer science');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `commentid` int(10) NOT NULL AUTO_INCREMENT,
  `blogid` int(10) NOT NULL,
  `accid` int(10) NOT NULL,
  `comment` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`commentid`, `blogid`, `accid`, `comment`, `time`) VALUES
(1, 1, 1, 'nyc blog ', '2014-04-22 23:32:13'),
(2, 2, 1, 'good job!!!', '2014-04-22 23:32:57');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
