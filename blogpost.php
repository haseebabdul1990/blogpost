<?php
include "function.php";
session_start();




/*if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  $accid=$_SESSION['accid'];
  $tittle=$_POST['tittle'];
  $content=$_POST['content'];
  $category=$_POST['category'];
  $imagepath=$_POST['imagepath'];
  $query="insert into blog (accid,title,content,category,imagepath) values ('$accid','$tittle','$content','$category','imagepath')";
  mysql_query($query);
  header('Location: home.php');
}
*/


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
  </head>

  <body style="padding-top:50px;">

      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Start Bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="profile.php">Profile</a>
                    </li>
                    <li><a href="blogpost.php">Blog Post</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">

      <div class="starter-template">
        <h1>Blog Post</h1>
        <p class="lead">Start blogging..</p>

        <form class="form-horizontal" role="form" action="blogpostprocess.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="tittle" class="col-sm-2 control-label" >Tittle</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="title" placeholder="Tittle" name="tittle" style="width:400px;">
            </div>
          </div>


          
          <div class="form-group">
            <label for="category" class="col-sm-2 control-label" >Category</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="category" placeholder="Category" name="category" style="width:300px;">
            </div>
          </div>


          <div class="form-group">
            <label for="content" class="col-sm-2 control-label">Content</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="content" style="width:700px;height:400px;" placeholder="Write here."></textarea>
              </div>
          </div>
          <div class="form-group">
            <label for="imagepath" class="col-sm-2 control-label">Cover</label>
            <div class="col-sm-10">
              <input type="file" id="imagepath" name="file" size="50">
            </div>
          </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Post</button>
                <button type="reset" class="btn btn-default">Reset</button>
              </div>
            </div>
        </form>






      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
  </body>
</html>