-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 28, 2014 at 07:38 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blogpost`
--
CREATE DATABASE IF NOT EXISTS `blogpost` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `blogpost`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `accid` int(10) NOT NULL AUTO_INCREMENT,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `afilename` varchar(50) NOT NULL,
  `afilepath` varchar(100) NOT NULL,
  `dob` date DEFAULT NULL,
  `sex` text,
  `about` text,
  PRIMARY KEY (`accid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`accid`, `firstname`, `lastname`, `email`, `username`, `password`, `afilename`, `afilepath`, `dob`, `sex`, `about`) VALUES
(1, 'Shoab', 'Ahmed', 'shoaba@clemson.edu', 'shoab101', '123456', '', '', '1991-03-25', NULL, 'Computer Science Graduate Student'),
(2, 'Abdul', 'Haseeb', 'ahaseeb@clemson.edu', 'haseeb101', '123456', 'Koala.jpg', 'uploads/haseeb101/', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`aid`, `username`, `password`) VALUES
(1, 'admin', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `blogid` int(10) NOT NULL AUTO_INCREMENT,
  `accid` int(10) NOT NULL,
  `tittle` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `filename` varchar(50) NOT NULL,
  `filepath` varchar(50) NOT NULL,
  `category` text NOT NULL,
  PRIMARY KEY (`blogid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blogid`, `accid`, `tittle`, `content`, `time`, `filename`, `filepath`, `category`) VALUES
(4, 2, 'Robots of the Future', 'Security blocks web threats to reduce malware infections, decrease help desk incidents and free up valuable IT resources. It has more than 100 security and filtering categories, hundreds of web application and protocol controls, and 60-plus reports with customization and role-based access. You can easily upgrade to Web Security Gateway when desired to get social media controls, SSL inspection, data loss prevention (DLP) and inline, real-time security from Websense ACE ', '2014-04-28 00:33:57', 'Jellyfish.jpg', 'uploads/haseeb101/', 'Robotics'),
(5, 1, 'Computers of the World', 'A computer is a general purpose device that can be programmed to carry out a set of arithmetic or logical operations automatically. Since a sequence of operations can be readily changed, the computer can solve more than one kind of problem.\r\nConventionally, a computer consists of at least one processing element, typically a central processing unit (CPU), and some form of memory. The processing element carries out arithmetic and logic operations, and a sequencing and control unit can change the order of operations in response to stored information. Peripheral devices allow information to be retrieved from an external source, and the result of operations saved and retrieved.', '2014-04-28 00:51:05', 'Tulips.jpg', 'uploads/shoab101/', 'computer science');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `commentid` int(10) NOT NULL AUTO_INCREMENT,
  `blogid` int(10) NOT NULL,
  `accid` int(10) NOT NULL,
  `comment` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`commentid`, `blogid`, `accid`, `comment`, `time`) VALUES
(1, 1, 1, 'nyc blog ', '2014-04-22 23:32:13'),
(2, 2, 1, 'good job!!!', '2014-04-22 23:32:57'),
(3, 2, 1, 'hello world', '2014-04-23 01:33:32'),
(4, 2, 1, 'hello world', '2014-04-23 01:33:48'),
(5, 2, 1, 'hello world', '2014-04-23 01:34:20'),
(6, 2, 1, 'Give me some sunshine', '2014-04-23 01:35:32'),
(7, 1, 1, 'Hello Boys!!', '2014-04-23 01:35:48'),
(8, 4, 1, 'Good Resource for learning.', '2014-04-28 00:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `currentusers`
--

CREATE TABLE IF NOT EXISTS `currentusers` (
  `accid` int(10) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `browser` text NOT NULL,
  `logintime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`accid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currentusers`
--

INSERT INTO `currentusers` (`accid`, `ip`, `browser`, `logintime`) VALUES
(2, '127.0.0.1', 'Google Chrome', '2014-04-28 00:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE IF NOT EXISTS `follow` (
  `fid` int(10) NOT NULL AUTO_INCREMENT,
  `accid` int(10) NOT NULL,
  `faccid` int(10) NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`fid`, `accid`, `faccid`) VALUES
(1, 2, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
